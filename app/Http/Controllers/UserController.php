<?php

namespace App\Http\Controllers;

use App\User;
use App\Post;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $user = User::all();
        abort(404);
    }

    public function show($id)
    {
        $user = User::findOrFail($id);        
        return view('user.profile')->with('user', $user);
    }
}
