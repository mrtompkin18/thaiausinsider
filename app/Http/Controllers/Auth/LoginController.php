<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\User;
use Socialite;
use File;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

     /**
     * Redirect the user to the provider authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider = 'facebook')
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider = 'facebook')
    {
        $user = Socialite::driver($provider)->stateless()->user();
        auth()->login($this->createUser($user, $provider)); 

        return redirect()->to('/home');
    }

    function createUser($providerUser, $provider){
        $user = User::where('provider_id', $providerUser->getId())->first();
        if (!$user) {
            $imagePath = "https://graph.facebook.com/v2.6/".$providerUser->getId()."/picture?type=large";
            $fileContents = file_get_contents($imagePath);
            $filePath = '/uploads/profile/' . $providerUser->getId() . ".jpg";
            File::put(public_path() . $filePath, $fileContents);

            $create['name'] = $providerUser->getName();
            $create['email'] = $providerUser->getEmail();
            $create['provider'] = $provider;
            $create['provider_id'] = $providerUser->getId();
            $create['profile_avatar'] = $filePath;
            
            $user = User::create($create);
        }
        
        return $user;
    }
}
