<?php

namespace App\Http\Controllers;

use App\Post; 
use App\PostImage; 
use App\PostComment; 
use Illuminate\Http\Request;
use Validator;
use Response;
use Auth;
use View;

class PostController extends Controller
{
    public function construct(){
        // $this->middleware("auth");
    }
    
    public function index()
    {
        
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required|string|max:1000',
            'images.*' => 'mimes:jpg,jpeg,png|max:20000'
        ],[
            'images.*.max' => 'The images may not be greater than :max kilobytes.',
            'images.*.mimes' => 'The images must be a file of type: jpg, jpeg, png.'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages()->all());   
        }

        $post = new Post();
        $post->user_id = Auth::user()->id;
        $post->content = nl2br($request->input('comment'));
        $post->save();

        if($files = $request->file('images')){
            $width = 1500;
            foreach ($files as $file){
                $filename = uniqid() . time() . '.' .$file->getClientOriginalExtension();
                $path = public_path('uploads/post/' . $filename);
                $instanceImage = \Image::make($file->getRealPath());
                if($instanceImage->width() > $width){
                    $instanceImage->resize($width, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
        
                $postImage = new PostImage();
                $postImage->post_id = $post->id;
                $postImage->image_path = 'uploads/post/' . $filename;
                $postImage->save();
                $instanceImage->save($path);
            }
        }
        return back()->with("success",'posted');
    }

    public function saveComment(Request $request){
        $response = array();

        $validator = Validator::make($request->all(),[
            "id"=> "required|integer",
            "comment" => "required|string|max:1000",
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages()->all()); 
        }

        $comment = new PostComment();
        $comment->comment_user_id = Auth::user()->id;;
        $comment->content = $request->input('comment');
        $comment->post_id = $request->input('id');
        
        $html = "";
        if($comment->save()){
            $post = Post::find($comment->post_id);
            $comments = PostComment::where("post_id", $comment->post_id)
                                    ->whereNull("is_sub_comment")
                                    ->orderBy('created_at','desc')
                                    ->get();

            foreach($comments as $comment){
                $html .= View::make('post.comment.comment-card', compact('comment','post'));
            }
        }

        $response['html'] = $html; 
        return Response::json($response); 
    }

    public function saveReply(Request $request){
        $response = array();

        $validator = Validator::make($request->all(),[
            "post_id"=> "required|integer",
            "comment_id"=> "required|integer",
            "comment" => "required|string|max:1000",
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages()->all()); 
        }

        $comment = new PostComment();
        $comment->comment_user_id = Auth::user()->id;;
        $comment->post_id = $request->input('post_id');
        $comment->is_sub_comment = $request->input('comment_id');
        $comment->content = $request->input('comment');

        $html = "";
        if($comment->save()){
            $post = Post::find($comment->post_id);
            $replies = PostComment::where("post_id", $comment->post_id)
                                    ->where("is_sub_comment", $comment->is_sub_comment)
                                    ->orderBy('created_at','desc')
                                    ->get();

            foreach($replies as $reply){
                $html .= View::make('post.comment.reply-comment-card', compact('reply'));
            }
        }

        $response['html'] = $html; 
        return Response::json($response); 
    }

    public function fetchComment(Request $request){
        $current_page = intval($request->input('current_page'));
        $post_id = $request->input("post_id");

        $postComment = PostComment::where('post_id', $post_id)->whereNull("is_sub_comment");

        $limit = 10;
        $start = ($current_page - 1) * $limit;    
        $total = $postComment->count();
        
        $response = array();
        $html = "";

        $post = Post::find($post_id);
        $comments = $postComment
                    ->orderbyDesc('created_at')
                    ->skip($start)
                    ->take($limit)
                    ->get();

        foreach($comments as $comment){
            $html .= View::make('post.comment.comment-card', compact('comment','post'));
        }

        $response['html'] = $html;
        $response['current_page'] = $current_page;
        $response['limit'] = $limit;
        $response['start'] = $start;
        $response['total'] = $total;
        return Response::json($response); 
    }

    public function fetchPost(Request $request){
        $current_page = intval($request->input('current_page'));

        $limit = 5;
        $start = ($current_page - 1) * $limit;    
        $total = Post::count();
        
        $response = array();
        $html = "";

        $posts = Post::orderbyDesc('created_at')
                    ->skip($start)
                    ->take($limit)
                    ->get();

        foreach($posts as $post){
            $html .= View::make('post.post-card', compact("post"));
        }

        $response['html'] = $html;
        $response['current_page'] = $current_page;
        $response['limit'] = $limit;
        $response['start'] = $start;
        $response['total'] = $total;
        return Response::json($response); 
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Post $post)
    {
        //
    }

    public function edit(Post $post)
    {
        //
    }

    public function update(Request $request, Post $post)
    {
        //
    }

    public function destroy(Post $post)
    {
        //
    }
}
