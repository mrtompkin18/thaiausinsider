<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";

    private $comment_limit = 3;

    public function images()
    {
        return $this->hasMany('App\PostImage', 'post_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\PostComment', 'post_id' , 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function comment_limit(){
        return $this->comment_limit;
    }
}
