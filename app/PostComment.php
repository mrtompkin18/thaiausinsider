<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    protected $table = "post_comments";

    public function user()
    {
        return $this->belongsTo('App\User','comment_user_id');
    }

    public function post(){
        return $this->belongsTo('App\Post', 'post_id');
    }

    public function get_sub_comment($comment_id) {
        return $this->where('is_sub_comment',$comment_id)
                    ->orderBy('created_at','desc')
                    ->get();
    }
}
