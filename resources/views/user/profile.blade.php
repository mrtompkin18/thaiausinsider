@extends('layouts.app')
@section('content')
<div class="_container">
    <div class="profile-banner" style="background-image:url('../uploads/banner/2568569746569891.png')">
        <div class="profile-wrap">
            <img class="rounded-circle" src="{{$user->profile_avatar}}" alt="" width="126">
            <div class="profile">
                <div class="name"> {{$user->name}} </div>
                <div class="city">{{$user->city}}</div>
            </div>
        </div>
    </div>
    <div class="profile-follow-wrap">
        <div class="follow-detail">
            <div class="follower">100 ผู้ติดตาม</div>
            <div class="following">100 กำลังติดตาม</div>
        </div>
        <div class="follow-btn">+ ติดตาม</div>
    </div>
    <div class="post-timeline my-3">
        @php
        $id = app('request')->segment(2);
        @endphp
        @if (Auth::user()->id == $id)
            @include('post.input-primary')
        @endif
        <div class="post-list"></div>
        <div class="spinner-loading m-3 text-center">
            <i class="fa fa-spinner fa-spin"></i>&nbsp;กำลังโหลดข้อมูล...
        </div>
    </div>
</div>
@endsection      
