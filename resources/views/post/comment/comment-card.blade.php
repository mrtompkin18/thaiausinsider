<div class="comment-panel fadeIn animated">
    <div class="d-flex">
        <a href="#">
            <img class="rounded-circle" src="{{$comment->user->profile_avatar}}" alt="" width="38">
        </a>
        <div class="flex-grow-1 pl-2">
            <div class="comment-panel-body mt-2">
                <div class="comment-detail">
                    <div class="comment-author">{{$comment->user->name}}</div>
                    <p class="m-0 text-dark">{!! $comment->content !!}</p>
                </div>
            </div>
            <div class="m-auto">
                <span class="comment-date">{{$comment->created_at->diffForHumans()}}</span>
                <span class="reply ml-2" onclick="$('.sub-comment-input-{{$comment->id}} .sub-comment-input').removeClass('d-none');">ตอบกลับ</span>
            </div>
            <!-- Reply Comment -->
            <div class="sub-comment-input-{{$comment->id}}">
                @php
                    $comment_count = count($comment->get_sub_comment($comment->id));
                @endphp
                {{-- @if ($comment_count)
                    <a href="javscript:avoid(0);" class="see-more-sub-reply m-2 mb-1 d-block" onclick="$(this).remove();$('.sub-comment-input-{{$comment->id}} .sub-comment-panel-section').removeClass('d-none');"><i class="fa fa-reply"></i> ดู {{$comment_count}} ความคิดเห็นย่อยเพิ่มเติม...</a>
                @endif --}}
                <div class="sub-comment-input mt-2 d-none">
                    @include("post.comment.input-reply")
                </div>
                <div class="sub-comment-panel-section">
                    @foreach ($comment->get_sub_comment($comment->id) as $reply)
                        @include('post.comment.reply-comment-card')
                    @endforeach
                </div>
            </div>
            <!-- End Reply Comment -->
        </div>
    </div>
</div>