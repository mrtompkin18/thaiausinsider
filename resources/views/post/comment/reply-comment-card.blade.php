<div class="comment-panel fadeIn animated mt-1">
    <div class="d-flex">
        <a href="#">
            <img class="rounded-circle" src="{{$reply->user->profile_avatar}}" alt="" width="24">
        </a>
        <div class="flex-grow-1 pl-2">
            <div class="comment-panel-body">
                <div class="comment-detail">
                    <span class="comment-author">{{$reply->user->name}}</span>
                    {!! $reply->content !!}
                </div>
            </div>
            <span class="comment-date ml-2">{{$reply->created_at->diffForHumans()}}</span>
        </div>
    </div>
</div>
