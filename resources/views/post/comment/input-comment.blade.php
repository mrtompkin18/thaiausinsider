@if (Auth::check())
<div class="d-flex fadeIn animated">
    <a href="#">
        <img class="rounded-circle" src="{{Auth::user()->profile_avatar}}" width="38">
    </a>
    <div class="flex-grow-1 pl-2">
        <textarea id="textarea-comment" class="p-2" rows="1" placeholder="แสดงความคิดเห็นของคุณ"></textarea>
    </div>
    <div class="text-center pl-2 pt-2">
        <div onclick="submitComment({{$post->id}})"><i class="fa fa-paper-plane"></i></div>
    </div>
</div>
@endif
