@php
$comment_limit = $post->comment_limit();
$comment_total = $post->comments()->whereNull("is_sub_comment")->count();
$comment_seemore_number = $comment_total - $comment_limit;
@endphp

<div class="post-panel">
    <div class="post-panel-header d-flex">
        <a href="#">
            <img class="rounded-circle" src="{{$post->user->profile_avatar}}" alt="" width="40">
        </a>
        <div class="pl-2">
            <div class="author">{{$post->user->name}}</div>
            <div class="comment-date">{{$post->created_at->diffForHumans()}}</div>
        </div>
    </div>
    <div class="post-panel-body">
        <article>{!! $post->content !!}</article>
        @if (count($post->images) > 0)
        <div class="fotorama mt-2">
            @foreach ($post->images as $image)
                <img id={{'image-'.$image->id}} src="{{asset($image->image_path)}}">
            @endforeach
        </div>
        @endif
    </div>
    <div class="post-panel-action">
        <div class="row">
            <div class="col-6 pr-0">
                <a href="javascript:void(0)" class="like text-light">
                    <i class="align-middle" data-feather="thumbs-up"></i>
                    <span class="pl-1 align-middle">ถูกใจ <span class="text-light count">(0)</span></span>
                </a>
            </div>
            <div class="col-6 pl-0">
                <a href="javascript:void(0)" class="comment text-light" data-toggle="modal" data-target="#show-post-{{$post->id}}">
                    <i class="align-middle" data-feather="message-square"></i>
                    <span class="pl-1 align-middle">แสดงความคิดเห็น</span>
                </a>
            </div>
        </div>
    </div>
    <div class="post-panel-footer p-0">
        <div class="post-write-comment panel-post-{{$post->id}}">
            @foreach($post->comments()->whereNull("is_sub_comment")->orderBy('created_at','desc')->take(1)->get() as $comment)
            <div class="comment-panel py-1 p-3 fadeIn animated">
                <div class="d-flex">
                    <a href="#">
                        <img class="rounded-circle" src="{{$comment->user->profile_avatar}}" alt="" width="38">
                    </a>
                    <div class="flex-grow-1 pl-2">
                        <div class="comment-panel-body mb-1">
                            <div class="comment-detail">
                                <div class="comment-author">{{$comment->user->name}}</div>
                                <p class="m-0 text-dark">{!! $comment->content !!}</p>
                            </div>
                        </div>
                        @if ($comment_total > 0)
                            <a href="javscript:avoid(0);" class="mt-2 d-block" onclick="fetchComment({{$post->id}})">ดู {{$comment_total - 1}} ความคิดเห็นเพิ่มเติม...</a>
                        @endif
                    </div>
                </div>
            </div>
           @endforeach
        </div>
    </div>
</div>
<!-- Modal -->
@include('post.post-modal')
