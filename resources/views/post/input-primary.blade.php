@if (Auth::check())
@if (session()->has('errors'))
    <div class="alert alert-danger">
        <ul class="m-auto">
            @foreach (session()->get('errors')->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
      {{session()->get('success')}}
    </div>
@endif
<form action="/post/create" id="post" name="post" method="post" enctype="multipart/form-data">
    @csrf
    <div class="post-comment-box p-3 d-flex flex-column">
        <div class="d-flex flex-row">
            <img class="rounded-circle" src="{{Auth::user()->profile_avatar}}" alt="" width="40" height="40">
            <textarea class="ml-3 flex-grow-1" name="comment" placeholder="Write a comment"></textarea>
        </div>
        <div class="post-send-wrap">
            <div class="preview-upload-image">
                <div class="row"></div>
            </div>
            <span class="image-upload">
                <input class="input-image-upload d-none" type="file" name="images[]" accept="image/*" multiple />
                <i data-feather="image"></i>
                <span class="ml-1 align-middle"> อัพโหลดภาพ <span class="limit-image-upload">(0/8)</span></span>
            </span>
            <div class="post-send-btn float-right" onclick="document.getElementById('post').submit();">
                <i class="fa fa-paper-plane"></i></i>
            </div>
        </div>
    </div>
</form>
@endif
