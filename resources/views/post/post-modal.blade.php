<div class="modal fade" id="show-post-{{$post->id}}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="post-panel m-auto" style="border:none;box-shadow:none">
                        <div class="post-panel-header d-flex">
                            <a href="#">
                                <img class="rounded-circle" src="{{$post->user->profile_avatar}}" alt="" width="40">
                            </a>
                            <div class="pl-2">
                                <div class="author">{{$post->user->name}}</div>
                                <div class="comment-date">{{$post->created_at->diffForHumans()}}</div>
                            </div>
                        </div>
                    <div class="post-panel-body">
                        <article>{!! $post->content !!}</article>
                        @if (count($post->images) > 0)
                            <div class="fotorama mt-2">
                                @foreach ($post->images as $image)
                                    <img id={{'image-'.$image->id}} src="{{asset($image->image_path)}}">
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="post-panel-action">
                        <div class="row">
                            <div class="col-6 pr-0">
                                <a href="javascript:void(0)" class="like text-light">
                                    <i class="align-middle" data-feather="thumbs-up"></i>
                                    <span class="pl-1 align-middle">ถูกใจ <span class="text-light count">(0)</span></span>
                                </a>
                            </div>
                            <div class="col-6 pl-0">
                                <a href="javascript:void(0)" class="comment text-light">
                                    <i class="align-middle" data-feather="message-square"></i>
                                    <span class="pl-1 align-middle">แสดงความคิดเห็น</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="post-panel-footer">
                        <div class="post-write-comment panel-post-{{$post->id}}">
                            <div class="comment-input-section animated fadeIn">
                                @include('post.comment.input-comment')
                            </div>
                            <div class="comment-panel-section">
                                {{-- Render template by PostController@fetchComment --}}
                            </div>
                        </div>
                    </div>
                    <div class="load-more-comment mb-3 text-center" onclick="fetchComment({{$post->id}})">
                        <i class="fa fa-chevron-down"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
