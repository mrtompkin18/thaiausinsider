require('readmore-js');
require('fotorama/fotorama');
require('sweetalert');
require('jquery-easy-loading');

const INACTIVE = "inactive";
const ACTIVE = "active";

const jump = require('jump.js');
window.jumpTo = function (target) {
    jump.default(target, {
        duration: 2000,
        a11y: true,
        offset: -200
    })
}

const aos = require('aos');
aos.init({
    duration: 600,
    easing: 'linear',
    delay: 100
});

const feather = require('feather-icons');
feather.replace();

$('article').readmore({
    collapsedHeight: 150,
    moreLink: '<a class="p-3" href="#">อ่านต่อ...</a>',
    lessLink: ''
});

$('textarea').keydown(autosize);

function autosize() {
    var el = this;
    setTimeout(function () {
        el.style.cssText = 'height:auto; padding:0';
        el.style.cssText = 'height:' + el.scrollHeight + 'px';
    }, 0);
}

$('.image-upload').click(function () {
    $('.input-image-upload')[0].click();
})

limitUploadImage = 8;
$('.input-image-upload').change(function () {
    var files = event.target.files;
    if (getCurrentImageUploaded() + files.length > limitUploadImage || files.length > limitUploadImage) {
        swal("จำกัดจำนวนอัพโหลดรูปภาพ 8 รูป", "กรุณาลบรูปภาพที่อัพโหลดไปแล้ว หากท่านต้องการที่จะอัพโหลดรูปใหม่", "error");
        return;
    }

    _.each(files, function (file) {
        getBase64(file).then((base64) => {
            $('.preview-upload-image .row').append(getTemplateImagePreview(base64));
            $('.limit-image-upload').html("(" + getCurrentImageUploaded() + "/" + limitUploadImage + ")");
        })
    })
})

function getCurrentImageUploaded() {
    return $('.image-preview').length;
}

function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

function getTemplateImagePreview(fileBase64) {
    return '<div class="col-4 col-md-3 mb-3 px-1"><div class="image-preview" style="background:url(' + fileBase64 + ')"><i class="fa fa-times" onclick="$(this).parent().parent().remove();$(\'.limit-image-upload\').html(\'(\' + $(\'.image-preview\').length + \'/' + limitUploadImage + ')\');"></i></div></div>'
}

window.fetchComment = function (postId) {
    var currentPage = localStorage.getItem('comment_next_page');
    if (!currentPage) localStorage.setItem("comment_next_page", 1);

    var data = new FormData();
    data.append("post_id", postId);
    data.append("current_page", localStorage.getItem('comment_next_page'));

    $.ajax({
        method: 'post',
        url: BASE_URL + '/post/fetch/comment',
        timeout: 5000,
        data: data,
        contentType: false,
        cache: false,
        processData: false,
        headers: {
            'X-CSRF-TOKEN': CSRF
        },
    }).then(function (response) {
        $("#show-post-" + postId + "").modal();
        $(".modal-dialog .panel-post-" + postId + " .comment-panel-section").append(response.html);
        localStorage.setItem("comment_next_page", response.current_page + 1);

        var remaining = response.total - response.start;
        if (remaining < response.limit) {
            $(".load-more-comment").hide();
        }
        
        handleOnModalClosed();
        fotoramaInit();
    }).fail(function () {
        swal("เกิดข้อผิดพลาด", "กรุณาเข้าสู่ระบบใหม่อีกครั้ง", "error")
    })
}

window.fetchPost = function (current_page) {
    localStorage.setItem("post_fetching", ACTIVE);

    var data = new FormData();
    data.append("current_page", current_page);

    $.ajax({
        method: 'post',
        url: BASE_URL + '/post/fetch/post',
        timeout: 5000,
        data: data,
        contentType: false,
        cache: false,
        processData: false,
        headers: {
            'X-CSRF-TOKEN': CSRF
        },
    }).then(function (response) {
        setTimeout(function () {
            localStorage.setItem("post_fetching", INACTIVE);
            if ((response.total - response.start) < response.limit) {
                $(".spinner-loading").fadeOut(600);
            } else {
                $(".post-list").append(response.html);
                fotoramaInit();
                localStorage.setItem("post_next_page", response.current_page + 1);
            }
        }, 1000);
    }).fail(function (e) {
        swal("เกิดข้อผิดพลาด", "กรุณาเข้าสู่ระบบใหม่อีกครั้ง", "error")
    })
}

window.submitComment = function (postId) {
    var comment = $(".panel-post-" + postId + " #textarea-comment");

    var data = new FormData();
    data.append('id', postId);
    data.append('comment', comment.val());

    if (!comment.val().trim()) {
        swal("ข้อมูลไม่ครบ", "กรุณาใส่ข้อความการแสดงความคิดเห็น", "error")
    } else {
        $.ajax({
            method: 'post',
            url: BASE_URL + '/post/comment',
            timeout: 5000,
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': CSRF
            },
        }).then(function (response) {
            comment.val('');
            fetchComment(postId);
        })
    }
}

window.submitReply = function (postId, commentId) {
    var comment = $(".sub-comment-input-" + commentId + " #textarea-reply");

    var data = new FormData();
    data.append('post_id', postId);
    data.append('comment_id', commentId);
    data.append('comment', comment.val());

    if (!comment.val().trim()) {
        swal("ข้อมูลไม่ครบ", "กรุณาใส่ข้อความการแสดงความคิดเห็น", "error")
    } else {
        $.ajax({
            method: 'post',
            url: BASE_URL + '/post/reply',
            timeout: 5000,
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': CSRF
            },
        }).then(function (render) {
            comment.val('')
            $(".sub-comment-input-" + commentId + " .sub-comment-panel-section").html(render.html);
        })
    }
}

$(document).init(function () {
    localStorage.clear();

    var current_page = localStorage.getItem("post_next_page");
    if (!current_page) current_page = 1;
    fetchPost(current_page);
});

$(window).scroll(function () {
    var scroll = $(document).height() - $(window).height() - $(window).scrollTop();
    var post_fetching = localStorage.getItem("post_fetching");
    if (scroll === 0 && post_fetching === INACTIVE) {
        var current_page = localStorage.getItem("post_next_page");
        fetchPost(current_page);
    }
});

window.handleOnModalClosed = function () {
    $("[id*=show-post]").on('hidden.bs.modal', function () {
        $(".modal-dialog .comment-panel-section").html("");
        $(".load-more-comment").show();
        localStorage.removeItem("comment_next_page");
    });
}

window.fotoramaInit = function () {
    return $(".fotorama").fotorama({
        width: '100%',
        height: 580,
        allowfullscreen: 'native',
        nav: false,
        margin: 0,
        fit: 'cover',
        spinner: {
            lines: 13,
            color: 'rgba(0, 0, 0, .75)'
        }
    });
}
