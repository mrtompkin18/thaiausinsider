<?php

Auth::routes();

// Home
Route::get('/home', 'HomeController@index');

// User
Route::get('/user/{id}', 'UserController@show')->name('user');

// Login
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');

// Post
Route::post('post/create', 'PostController@create');
Route::get('post/list', 'PostController@index');
Route::post('post/comment/more', 'PostController@loadComment');
Route::post('post/comment', 'PostController@saveComment');
Route::post('post/reply', 'PostController@saveReply');

Route::post('post/fetch/comment', 'PostController@fetchComment');
Route::post('post/fetch/post', 'PostController@fetchPost');
